/**
 * Created by IntelliJ Idea
 * User: jzbrooks
 * Date: 3/10/14
 * Time: 9:08 AM
 *
 * Abstract class to support heap operation implementation
 * Implements preorder traversal method
 */
abstract public class Heap {
    abstract void preorder();
    abstract void Min_insert(int k);
    abstract void Max_insert(int k);
    abstract int removeMin();
    abstract int removeMax();

    public void preorder(Node p) {
        if(p==null) return;
        System.out.print("(");
        System.out.print(p.key);
        if(p.left!=null) System.out.print(" ");
        preorder(p.left);
        if(p.right!=null) System.out.print(" ");
        preorder(p.right);
        System.out.print(")");

    }
}

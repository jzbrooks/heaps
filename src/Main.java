import java.io.*;

/**
 * Created by IntelliJ Idea
 * User: jzbrooks
 * Date: 3/10/14
 * Time: 9:01 AM
 *
 * Main driver class
 */
public class Main {

    public static void main(String[] args) {
        String heap_type = "", ordering = "", command = "", instruction = "", item = "";
        Heap heap = null;

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        try {
            heap_type = in.readLine();
            ordering = in.readLine();
        } catch (IOException ex) {
            System.out.println("Error while reading tree type");
        }

        if (heap_type.equals("Leftist")) { heap = new LEFTIST_Heap(); }
        else if (heap_type.equals("Skew")) { heap = new SKEW_Heap(); }

        try {

            while ((command = in.readLine()) != null) {
                instruction = item = "";
                if(command.contains("Insert")) {
                    for (int i=0; i<command.length(); i++) {
                        if (command.charAt(i) == ' ') {
                            instruction = command.substring(0, i).trim();
                            item = command.substring(i + 1, command.length()).trim();
                        }
                    }
                }
                else instruction = command;
                if(heap!=null) {
                    if (instruction.equals("Insert") && ordering.equals("Min")) {
                        heap.Min_insert(Integer.parseInt(item));
                    } else if (instruction.equals("Insert") && ordering.equals("Max")) {
                        heap.Max_insert(Integer.parseInt(item));
                    } else if (instruction.equals("RemoveMin") && ordering.equals("Min")) {
                        System.out.print(String.format("%d: ",heap.removeMin()));
                    } else if (instruction.equals("RemoveMax") && ordering.equals("Max")) {
                        System.out.print(String.format("%d: ",heap.removeMax()));
                    }
                    heap.preorder();
                }
                System.out.println();
            }

        } catch (IOException ex) {
            System.out.println("Error while reading commands");
        }
    }
}

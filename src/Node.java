/**
 * Created by IntelliJ Idea
 * User: jzbrooks
 * Date: 3/10/14
 * Time: 9:01 AM
 *
 * Node class for binary, linked tree style heap
 */
public class Node {
    int key, null_len;
    Node left, right;

    Node() {left=right=null;}
    Node(int k) {key=k; left=right=null;}
}

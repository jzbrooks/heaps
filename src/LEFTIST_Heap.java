/**
 * Created by IntelliJ Idea
 * User: jzbrooks
 * Date: 3/10/14
 * Time: 9:08 AM
 *
 * Binary heap class that supports leftist balancing and either
 * min or max ordering
 * Extends heap class to inherit virtual methods
 */
public class LEFTIST_Heap extends Heap {
    Node root;

    public LEFTIST_Heap() {root=null;}

    @Override
    public void Min_insert(int k) {
        root = mergeMin(new Node(k), root);
    }

    @Override
    public void Max_insert(int k) {
        root = mergeMax(new Node(k), root);
    }

    @Override
    public int removeMin() {
        int x = root.key;
        root = mergeMin(root.left, root.right);
        return x;
    }

    @Override
    public int removeMax() {
        int x = root.key;
        root = mergeMax(root.left, root.right);
        return x;
    }

    @Override
    public void preorder() {
        preorder(root);
    }

    private Node mergeMin(Node n1, Node n2) {
        if(n1==null)
            return n2;
        if(n2==null)
            return n1;
        if(n1.key < n2.key)
            return mergeMin_help(n1, n2);
        else
            return mergeMin_help(n2, n1);

    }

    private Node mergeMin_help(Node n1, Node n2) {
        if(n1.left==null) {
            n1.left = n2;
        }
        else {
            n1.right = mergeMin(n1.right, n2);
            if(n1.left.null_len < n1.right.null_len) {
                Node l = n1.left;
                n1.left = n1.right;
                n1.right = l;
            }
            n1.null_len = n1.right.null_len + 1;
        }
        return n1;
    }

    private Node mergeMax(Node n1, Node n2) {
        if(n1==null)
            return n2;
        if(n2==null)
            return n1;
        if(n1.key > n2.key)
            return mergeMax_help(n1, n2);
        else
            return mergeMax_help(n2, n1);

    }

    private Node mergeMax_help(Node n1, Node n2) {
        if(n1.left==null) {
            n1.left = n2;
        }
        else {
            n1.right = mergeMax(n1.right, n2);
            if(n1.left.null_len < n1.right.null_len) {
                Node l = n1.left;
                n1.left = n1.right;
                n1.right = l;
            }
            n1.null_len = n1.right.null_len + 1;
        }
        return n1;
    }
}
